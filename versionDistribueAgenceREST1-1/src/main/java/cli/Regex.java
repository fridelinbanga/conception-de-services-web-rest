package cli;

public class Regex {
	
	// Uniquement des lettres alphabetiques et des espaces
	public boolean estAlphabetique(String s) {
	    return s.matches("[a-z A-Z]+");
	}
	
	
	public boolean estUneDate(String s) {
	    return s.matches("(0[1-9]|1[0-9]|2[0-9]|3[0-1]){1}/(0[1-9]|1[0-2]){1}/(202)[2-9]{1} (0[0-9]|1[0-9]|2[0-3]){1}:(0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]){1}");
	}
	
	// XXXXXXXXXXXXXXXX XXX MM/AA
	public boolean estUnCodeCB(String s) {
		return s.matches("(\\d{16}) (\\d{3}) (0[1-9]|1[0-2]){1}/2[2-9]{1}");
	}
}
