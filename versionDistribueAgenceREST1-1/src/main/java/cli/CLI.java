package cli;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import models.ConvertImage;
import models.ConvertLocalDateTime;
import models.InfosHotel;
import models.Offre;
import models.RechercheDTO;
import models.ReserveDTO;
import repositories.InfosHotelRepository;

@Component
public class CLI implements CommandLineRunner{	
	
	public static String enleverLaSensiCasse(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
	}
		
	@Autowired
	private InfosHotelRepository repository;
	@Autowired
	private RestTemplate proxy;
	
	public void initBDD() {
		repository.save(new InfosHotel("HôtelF1", "Montpellier","http://localhost:8000/hotel/api/", "123",1.10f,3,"France","Avenue de Saint-Hubert"));
		repository.save(new InfosHotel("Hôtel Transcontinental", 
				"Paris","http://localhost:8001/hotel/api/", "345",1.25f,2,"France","Avenue Nina-Simone"));
		repository.save(new InfosHotel("Grand Hôtel du Lac", 
				"Genève","http://localhost:8002/hotel/api/", "567",1.18f,5,"Suisse","Rue Italie 1"));
	}
		
	@Override
	public void run(String... args) throws Exception {
		
		initBDD();

		Scanner sc= new Scanner(System.in);
		Regex rg= new Regex();
		String choix;
		ConvertLocalDateTime pDT = new ConvertLocalDateTime(); //Utiliser pour transformer un String en LocalDateTime et inversement
		ConvertImage cI = new ConvertImage(); //Utiliser pour ouvrir l'image
				
		System.out.println("Bienvenue ! ");

		while (true) {
			System.out.println("Veuillez sélectionner une option ! ");
			System.out.println("1 - Rechercher une chambre d'hôtel ");
			System.out.println("0 - Exit");

			choix=sc.nextLine();
			switch(choix) {
			case "1":

				/* CHOIX VILLE SEJOUR*/
				System.out.println("Veuillez sélectionner la ville de votre séjour");

				String choixVille;
				choixVille=sc.nextLine();
				while (!rg.estAlphabetique(choixVille)) {           
					System.err.println("Mauvaise input /!\\ Veuillez entrer un nom de ville correct ");
					choixVille = sc.nextLine();
				}

				/* CHOIX DATE ARRIVEE*/
				System.out.println("Veuillez sélectionner votre date d'arrivée au format JJ/MM/AAAA HH:MM");

				String choixDateArrive;
				choixDateArrive=sc.nextLine();
				while (!rg.estUneDate(choixDateArrive)) {
					System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
					choixDateArrive=sc.nextLine();
				}

				String[] separeDateHeure= choixDateArrive.split(" ");
				String[] jjMMaaaa= separeDateHeure[0].split("/");
				String[] hhMM= separeDateHeure[1].split(":");
				LocalDateTime dateArrivee=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
						,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));

				/* CHOIX DATE DEPART*/
				System.out.println("Veuillez sélectionner votre date de départ au format JJ/MM/AAAA HH:MM");

				String choixDateDepart;
				choixDateDepart=sc.nextLine();
				while (!rg.estUneDate(choixDateDepart)) {
					System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
					choixDateDepart=sc.nextLine();
				}

				separeDateHeure= choixDateDepart.split(" ");
				jjMMaaaa= separeDateHeure[0].split("/");
				hhMM= separeDateHeure[1].split(":");
				LocalDateTime dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
						,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));

				while(dateDepart.isBefore(dateArrivee)) {             
					System.err.println("Mauvaise input /!\\ Veuillez entrer une date de départ supérieur à votre date d'arrivée");

					choixDateDepart=sc.nextLine();
					while (!rg.estUneDate(choixDateDepart)) {
						System.err.println("Mauvaise input /!\\ Veuillez entrer une date existante et au format correct");
						choixDateDepart=sc.nextLine();
					}

					separeDateHeure= choixDateDepart.split(" ");
					jjMMaaaa= separeDateHeure[0].split("/");
					hhMM= separeDateHeure[1].split(":");
					dateDepart=LocalDateTime.of(Integer.parseInt(jjMMaaaa[2]),Integer.parseInt(jjMMaaaa[1])
							,Integer.parseInt(jjMMaaaa[0]), Integer.parseInt(hhMM[0]),Integer.parseInt(hhMM[1]));
				} 

				/* CHOIX NBR PERSONNE A HEBERGER*/
				System.out.println("Veuillez sélectionner le nombre de personne à héberger dans la chambre");
				int nbrPers;
				while (true) {
					try{
						nbrPers= sc.nextInt();
						if (nbrPers<1 || nbrPers>6) {
							throw new IllegalArgumentException();               
						}
						break;                
					}catch(Exception e) {
						System.err.println("Mauvaise input /!\\ Veuillez entrer un nombre de personne positif "
								+ "et réaliste pour une seule chambre");
						sc.nextLine();
					}
				}

				/* CONSULTATION DES OFFRES AUPRES DES HOTELS PARTENAIRES  */
				
				int cpt_nbr_hotels_trouvee=0;
				List<InfosHotel> infH = new ArrayList<>();
				for (InfosHotel iH : repository.findAll()) {
					if (iH.getVilleHotel().equals(enleverLaSensiCasse(choixVille))) {
						infH.add(iH);
						cpt_nbr_hotels_trouvee++;
					}
					
				}			

				if (cpt_nbr_hotels_trouvee==0){
					System.out.println("Aucun hôtel correspondant à vos critères n'a été trouvé chez nos partenaires.");
					System.out.println("Merci et à bientôt !");
					System.exit(0);
					return;
				}
				
				
				
				List<InfosHotel> infHotel = new ArrayList<>();
				List<Offre[]> listeOffresparhotel = new ArrayList<>();	
				for (InfosHotel iH : infH) {
					String uri = iH.getUrlWS() + "/recherche";
					
					RechercheDTO recherche=new RechercheDTO(2,iH.getMdpWS(),pDT.DateToString(dateArrivee), pDT.DateToString(dateDepart), nbrPers);
					Offre[] listeOffres = proxy.postForObject(uri,recherche,Offre[].class);
					
					if (listeOffres != null) {					
						infHotel.add(iH);
						listeOffresparhotel.add(listeOffres);
					}
				}
				
				if (infHotel.isEmpty()) {
					System.out.println("Aucune chambre correspondant à vos critères n'a été trouvé chez nos partenaires.");
					System.out.println("Merci et à bientôt !");
					System.exit(0);
					return;
				}
				
				System.out.println("\n======================= RESULTATS TROUVES ===============================\n");
				
				for (int i =0;i<infHotel.size();i++) {
					System.out.println("Hôtel : "+infHotel.get(i).getNomHotel());
					System.out.println("Voici ses chambres disponibles :\n");
					for (Offre of :  listeOffresparhotel.get(i)) {
						 System.out.println("Numero de l'offre : "+of.getIdOffre()+", nombre de lits : "+of.getNbrLits()+", dateArrivee : "+
								    of.getDateArrivee()+" ,dateDepart : "+of.getDateDepart()+",prix : "+of.getPrix()*infHotel.get(i).getPercentAdd());		
					}
					System.out.println("\n");
				}
				
				System.out.println("==========================================================================\n");
				sc.nextLine();
				
				System.out.println("Voulez vous affichez les images des chambres d'hôtels trouvées ? [y/n] ");
				String choixAff;
				choixAff=sc.nextLine();
				
				ArrayList<String> choixYN= new ArrayList<>();
				choixYN.addAll(Arrays.asList("Y","y","n","N"));
				
				while(!choixYN.contains(choixAff)) {
					System.err.println("Mauvaise input /!\\ Veuillez répondre y ou n");
					choixAff=sc.nextLine();
				}
				
				switch(choixAff) {
				case "y":
					for (int i =0;i<infHotel.size();i++) {
						for (Offre of :  listeOffresparhotel.get(i)) {
							cI.StringToImage(of.getImgEncode(),"chambre id : "+Integer.toString(of.getIdOffre()));
							cI.afficherImage("chambre id : "+Integer.toString(of.getIdOffre()));
						}

					}					
					break;
				case "Y":
					for (int i =0;i<infHotel.size();i++) {
						for (Offre of :  listeOffresparhotel.get(i)) {
							cI.StringToImage(of.getImgEncode(),"chambre id : "+Integer.toString(of.getIdOffre()));
							cI.afficherImage("chambre id : "+Integer.toString(of.getIdOffre()));
						}

					}	
					break;
				case "n":
					break;
				case "N":
					break;
				}
				
				/* RESERVATION PARMIS LES CHOIX PROPROSEES  */
				/* 			CHOIX HOTEL  */
				System.out.println("Veuillez entrer le nom de l'ĥôtel à réserver : ");
				
				String choixHotel;
				choixHotel=sc.nextLine();
				InfosHotel hotelSelec=null;
				
				while(hotelSelec == null) {		
					for (InfosHotel iH :infHotel) {
						if(iH.getNomHotel().equals(choixHotel)) {
							hotelSelec=iH;
						}
					}					
						
					if (hotelSelec == null) {
						System.err.println("Ce nom ne fait pas partie de la liste proposée, êtes-vous sûr d'avoir saisi le bonne orthographe ?"
									+ " Veuillez réessayer une nouvelle fois");
						choixHotel=sc.nextLine();
					}
				}
				
				/* 			CHOIX CHAMBRE  */
				System.out.println("\nVeuillez entrer le numero de la chambre à réserver");
				
				String choixChambre; 
				choixChambre=sc.nextLine();
				boolean sortirb=true;
				String reference=null;
				
				while(sortirb) {	
					try {					
						
						for (int i =0;i<infHotel.size();i++) {
							if (!hotelSelec.getNomHotel().equals(infHotel.get(i).getNomHotel())) {
								continue;
							}
							for (Offre of :  listeOffresparhotel.get(i)) {
								if (of.getIdOffre() == Integer.parseInt(choixChambre)) {
									sortirb=false;
								}
							}

						}		
						
					} catch (NumberFormatException e) {
						
					}						
					if (sortirb) {
						System.err.println("Ce numero ne fait pas partie de la liste proposée pour votre hôtel, "
								+ "êtes-vous sûr d'avoir saisi le bon numéro d'offre ?"
								+ " Veuillez réessayer une nouvelle fois");									
						choixChambre=sc.nextLine();
					}
				}
				
				System.out.println("\n=========== Réservartion de l'hôtel \""+hotelSelec.getNomHotel()+"\" offre"
						+ " numéro "+choixChambre+" ============");
				
				System.out.println("Veuillez entrer le nom de la personne principale à héberger");
				
				String nom;
				nom=sc.nextLine();
				while (!rg.estAlphabetique(nom)) {						
					System.err.println("Mauvaise input /!\\ Veuillez entrer un nom correct");
					nom = sc.nextLine();
			    }
				
				System.out.println("Veuillez entrer le prénom de la personne principale à héberger");
				String prenom;
				prenom=sc.nextLine();
				while (!rg.estAlphabetique(prenom)) {						
					System.err.println("Mauvaise input /!\\ Veuillez entrer un prenom correct");
					prenom = sc.nextLine();
			    }
				
				System.out.println("Veuillez entrer le code de votre carte bancaire suivit du cyptogramme "
						+ "et de la date d'expiration au format suivant : XXXXXXXXXXXXXXXX XXX MM/AA");
										
				String codeCB;
				codeCB=sc.nextLine();
				while (!rg.estUnCodeCB(codeCB)) {						
					System.err.println("Mauvaise input /!\\ Veuillez entrer un code de carte bancaire correct "
							+ "suivant ce format : XXXXXXXXXXXXXXXX XXX MM/AA");
					codeCB = sc.nextLine();
			    }				
				
				
				/* APPEL AU WEBSERVICE DE RESERVATION  */

		
				String uri = hotelSelec.getUrlWS() + "/reserve";		

				ReserveDTO reserve=new ReserveDTO(2,hotelSelec.getMdpWS(),Integer.parseInt(choixChambre),nom,prenom,codeCB);
				
				reference = proxy.postForObject(uri,reserve,String.class);	
				
				System.out.println("\nRéservation effectuée, voici votre numéro de réference : "+reference);
				
				System.out.println("Merci à bientôt !\n");
				System.exit(0);
				break;
				
			case "0":
				sc.close();
				System.out.println("Merci à bientôt !");
				System.exit(0);
				return;
			
			default:
				System.err.println("Mauvaise input /!\\ Veuillez entrer le nombre 1 ou 0");
				break;
			}

		}
	}
}


