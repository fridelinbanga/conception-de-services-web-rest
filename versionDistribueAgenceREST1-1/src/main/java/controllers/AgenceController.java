package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import models.InfosHotel;
import models.Offre;
import models.OffreWS;
import models.RechercheDTO;
import models.RechercheWS_DTO;
import repositories.InfosHotelRepository;

@RestController
public class AgenceController {
	/* ATTRIBUTES */	
	private static final String uri = "agence/api";
	
	@Autowired
	private InfosHotelRepository repository;
	@Autowired
	private RestTemplate proxy;
	
	/*METHODS*/
	public static String enleverLaSensiCasse(String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
	}
	
	/*METHODS RESTS*/	
	@PostMapping(uri+"/recherche")
	public List<OffreWS> recherche(@RequestBody RechercheWS_DTO params){
		

		List<OffreWS> listeOffres= new ArrayList<>();	
		for (InfosHotel iH : repository.findAll()) {
			if (iH.getVilleHotel().equals(enleverLaSensiCasse(params.getVille()))) {
				
				if (iH.getNbrEtoiles()>=params.getTypeHotel()) {
				
					String uri = iH.getUrlWS() + "/recherche";
					
					RechercheDTO recherche=new RechercheDTO(2,iH.getMdpWS(),params.getDateDebut(), params.getDateFin(), params.getNbrPers());
					Offre[] offres = proxy.postForObject(uri,recherche,Offre[].class);
					
					for (Offre o :offres) {
						OffreWS oW= new OffreWS(iH.getNomHotel(),iH.getPays(),iH.getVilleHotel(),iH.getRue(),iH.getNbrEtoiles(),o.getNbrLits(),o.getPrix()*iH.getPercentAdd());
						listeOffres.add(oW);
					}
				}
				
			}
		}
		return listeOffres;
		
	}
	
}
