# Agence de Voyage et Hôtels - API REST avec Spring Boot

## Description du Projet
L'objectif de ce projet est de développer une application Java utilisant des **services web REST** pour les réservations d'hôtels en ligne. Cette application offre une interface permettant aux utilisateurs de saisir les informations suivantes : la ville de destination, la date d'arrivée, la date de départ, la plage de prix souhaitée, la catégorie de l'hôtel (nombre d'étoiles) et le nombre d'invités.

En réponse, l'application renvoie une liste d'hôtels correspondant aux critères de l'utilisateur, affichant les informations suivantes pour chaque hôtel : nom de l'hôtel, adresse de l'hôtel (pays, ville, rue, numéro, localité, coordonnées GPS), prix, classement en étoiles et le nombre de lits disponibles.

L'utilisateur sélectionne un hôtel dans la liste, et l'application demande des informations supplémentaires, notamment le nom du principal invité et les détails de la carte de crédit. L'application utilise ces informations pour finaliser la réservation de l'hôtel.

## Test de la Version Distribuée
Vous pouvez tester notre application depuis un IDE en suivant ces étapes :

1. Importez les 6 projets Maven suivants :

   - versionDistribueHotels
   - versionDistribueHotels-1
   - versionDistribueHotels-2
   - versionDistribueAgenceRest1
   - versionDistribueAgenceRest1-1
   - versionDistribueComparateur TRIVALGO REST

2. Démarrez les 3 serveurs un par un (exécutez le programme `StartServeursHotelsApplication` dans le package principal de chaque serveur).

3. Lancez l'interface de ligne de commande (CLI) d'une Agence (exécutez le programme `AgenceApplication` dans le package principal de l'Agence).

Pour tester le comparateur TRIVALGO, assurez-vous d'avoir démarré les 3 serveurs d'hôtels et les 2 CLIs des Agences. Ensuite, exécutez le programme `ComparateurTrivalgoApplication` dans le package principal du projet `versionDistribueComparateur TRIVALGO REST`.

## Diagramme UML du Projet

<p align="center">
  <img src="UML.png" alt="Diagramme UML" width="800">
</p>