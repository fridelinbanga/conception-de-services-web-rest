package models;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class ConvertImage {

	public byte[] ImageToString(String nomImage) throws IOException {
		byte[] fileContent = FileUtils.readFileToByteArray(new File("src/main/resources/static/"+nomImage+".jpg"));
		return fileContent;
	}
	
	public void StringToImage(byte[] codeImage,String nomImage) throws IOException {
		FileUtils.writeByteArrayToFile(new File("src/main/resources/static/"+nomImage+".jpg"), codeImage);
	}
	
	public void afficherImage(String nomImage) throws IOException {
		
		System.setProperty("java.awt.headless", "false");
	    BufferedImage img=ImageIO.read(new File("src/main/resources/static/"+nomImage+".jpg"));
        ImageIcon icon=new ImageIcon(img);
        JFrame frame=new JFrame(nomImage);
        frame.setLayout(new FlowLayout());
        frame.setSize(1280,720);
        JLabel lbl=new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
	}
}

