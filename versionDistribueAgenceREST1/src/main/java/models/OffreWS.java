package models;

public class OffreWS {
	
	private String nomHotel;
	private String paysHotel;
	private String villeHotel;
	private String rueHotel;
	private int nombreEtoile;
	private int nombreLits;
	private float prix;
	
	public OffreWS() {
		
	}	

	public OffreWS(String nomHotel, String paysHotel, String villeHotel, String rueHotel, int nombreEtoile,
			int nombreLits, float prix) {
		super();
		this.nomHotel = nomHotel;
		this.paysHotel = paysHotel;
		this.villeHotel = villeHotel;
		this.rueHotel = rueHotel;
		this.nombreEtoile = nombreEtoile;
		this.nombreLits = nombreLits;
		this.prix = prix;
	}


	public String getNomHotel() {
		return nomHotel;
	}
	public void setNomHotel(String nomHotel) {
		this.nomHotel = nomHotel;
	}
	public int getNombreEtoile() {
		return nombreEtoile;
	}
	public void setNombreEtoile(int nombreEtoile) {
		this.nombreEtoile = nombreEtoile;
	}
	public int getNombreLits() {
		return nombreLits;
	}
	public void setNombreLits(int nombreLits) {
		this.nombreLits = nombreLits;
	}
	public float getPrix() {
		return prix;
	}
	public void setPrix(float prix) {
		this.prix = prix;
	}

	public String getPaysHotel() {
		return paysHotel;
	}

	public void setPaysHotel(String paysHotel) {
		this.paysHotel = paysHotel;
	}

	public String getVilleHotel() {
		return villeHotel;
	}

	public void setVilleHotel(String villeHotel) {
		this.villeHotel = villeHotel;
	}

	public String getRueHotel() {
		return rueHotel;
	}

	public void setRueHotel(String rueHotel) {
		this.rueHotel = rueHotel;
	}

	

}
