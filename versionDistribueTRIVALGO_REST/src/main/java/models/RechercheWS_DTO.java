package models;

public class RechercheWS_DTO {
	private String ville;
	private String dateDebut;
	private String dateFin;
	private int nbrPers;
	private int typeHotel;
	
	public RechercheWS_DTO() {
		
	}
	
	public RechercheWS_DTO(String ville, String dateDebut, String dateFin, int nbrPers, int typeHotel) {
		super();
		this.ville = ville;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nbrPers = nbrPers;
		this.typeHotel = typeHotel;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(String dateDebut) {
		this.dateDebut = dateDebut;
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin;
	}

	public int getNbrPers() {
		return nbrPers;
	}

	public void setNbrPers(int nbrPers) {
		this.nbrPers = nbrPers;
	}

	public int getTypeHotel() {
		return typeHotel;
	}

	public void setTypeHotel(int typeHotel) {
		this.typeHotel = typeHotel;
	}
	
	

}
